from chess import chess_reset, chess_winner, chess_moveRandom, searchAlphaBeta, INFINITY
import chess
import pickle
from transposition import Trans, ZOBRIST_PIECES
from pybrain.datasets import SupervisedDataSet
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer

depth = 10
rounds = 100

def getInputs(state):
    inputs = []
    if chess.state.whoseTurn == "W":
        inputs = [1]
    else:
        inputs = [0]

    for row in chess.state.board:
        for square in row:
            inputs.append(ZOBRIST_PIECES[square])
    return inputs

def getEval(state, net):
    return net.activate(tuple(getInputs(state)))
def getNet():
    return pickle.load(open("neuralNet.pkl", 'rb'))
def saveNet(net):
    output = open('neuralNet.pkl', 'wb')
    pickle.dump(net, output)
    output.close()

def train():
    ds = SupervisedDataSet(31, 1)
    for i in range(0,rounds):
        chess_reset()
        while True:
            if chess_winner() != '?':
                break
            
            print(chess.state)
            inputs = getInputs(chess.state) 
            print(inputs)
            
            score = -searchAlphaBeta(depth, -INFINITY, INFINITY,chess.trans.getZobristVal(chess.state))
            ds.addSample(tuple(inputs), score)
            print(str(score) + "\n") 
            chess_moveRandom()
            #raw_input("...")

    #print data set
    for inpt, target in ds:
        print inpt, target
    print(len(ds))

    #tain network
    net = buildNetwork(31, 4, 1, bias=True)
    trainer = BackpropTrainer(net, ds)
    trainer.trainUntilConvergence()

    #save the network
    saveNet(net)
    #test our results
    tests = 0
    error = 0.0
    for i in range(0,1):
        chess_reset()
        while True:
            if chess_winner() != '?':
                break
            print(chess.state)
            inputs = getInputs(chess.state) 
            print(inputs)
            
            score = -searchAlphaBeta(depth, -INFINITY, INFINITY,chess.trans.getZobristVal(chess.state))
            estimatedScore = net.activate(tuple(inputs))
            error += abs(score - estimatedScore)

            print(str(score))
            print(str(estimatedScore) + "\n")
            chess_moveRandom()
            tests += 1
    accuracy = float(error) / float(tests)
    print(accuracy)

train()


