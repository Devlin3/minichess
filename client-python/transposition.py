import random, sys, copy

ZOBRIST_PIECES = {"p" : 0, "P" : 1, "r" : 2, "R" : 3, "n" : 4, "N" : 5, "b" : 6, "B" : 7, "q" : 8, "Q" : 9, "k" : 10, "K" : 11, "." : 12}
TRANS_SIZE = 2 ** 16 
EXACT = 1
LOWER = 0
UPPER = -1
EMPTY_NODE = (None, None, None, None)

class Trans:
    """Transpostion table class."""
    def __init__(self):
        """Initialize zobrist key constants"""
        self.zobWhite = random.randint(0,sys.maxint * 2)
        self.zobBlack = random.randint(0,sys.maxint * 2)
        self.zobVals = [[[0 for x in range(13)]for y in range(5)] for z in range(6)]
        for i in range (0,6):
            for j in range(0,5):
                for k in range(0,13):
                    self.zobVals[i][j][k] = random.randint(0,sys.maxint * 2)
        
        self.table = [EMPTY_NODE for x in range(TRANS_SIZE)]
    
    def getEval(self, zobVal, depth):
        """Check is entry is in the table
        parms:
            zobVal - int current state to check value for
            depth - How far down the search we are in
        Returns:
            eval of stored state or None if does not exits
        """
        oldKey, oldDepth, oldVal, flag = self.getElement(zobVal)
        if oldKey == zobVal and depth <= oldDepth:
            return oldVal
        return None
        
    def updateTrans(self, key, depth, val, flag):
        """decides to update table or not based on depth and random repalcement
        params:
            key - int - state zobrsit hash
            depth - int - level search is at
            val - int - state eval
            flag - int - state the value was found [UPPER, LOWER, EXACT]
        returns:
            None
        """
        oldKey, oldDepth, oldVal, oldFlag = self.table[key % TRANS_SIZE]
        if oldKey == None or depth >= oldDepth:
            self.table[key % TRANS_SIZE] = (key, depth, val, flag)

    
    def getElement(self,key):
        """Returns a table entry for a given zobrist hash"""
        return self.table[key % TRANS_SIZE]

    def getZobristVal(self, state):
        """Finds zobristh hash value for a state
        params:
            state - state object - current state
        returns:
            int - zobrist hash value
        """
        zobrist = 0
        if state.whoseTurn == "W":
            zobrist ^= self.zobWhite
        else:
            zobrist ^= self.zobBlack

        for row in range(0,len(state.board)):
            for col in range(0,len(state.board[row])):
                piece = state.board[row][col]
                pieceIndex = ZOBRIST_PIECES[piece]
                zobrist ^= self.zobVals[row][col][pieceIndex]
        return zobrist


    def zobristUpdate(self, oldVal,curState, move):
        """Incrimental update of zobrist hash based on move
        prams:
            oldVal - int - initial zobrist hash
            curState - state obj - original state
            move - move object - move to update hash
        returns:
            int - zobrist hash of next state
        """
        state = copy.deepcopy(curState)
        origSqr = move.fromSquare
        newSqr = move.toSquare
        
        #update based on piece movement
        for i in range(0,2):
            for row, col in [(origSqr.row, origSqr.col), (newSqr.row, newSqr.col)]:
                oldVal ^= self.zobVals[row][col][ZOBRIST_PIECES[state.board[row][col]]]
            #only update the move on fist iteration
            if i == 0:
                state.move(move)

        #update based on whose turn it is
        oldVal ^= self.zobWhite
        oldVal ^= self.zobBlack

        return oldVal
    def reset(self):
        """clear transposition table, but keep zobrist hash constanst"""
        self.table = [EMPTY_NODE for x in range(TRANS_SIZE)]


