import copy
from chess import WIN, LOSS, DRAW

#game state constants
BLANK = '.'
WHITE = "W"
BLACK = "B"
B_KING = "k"
W_KING = "K"
W_PAWN = "P"
B_PAWN = "p"
B_QUEEN = "q"
W_QUEEN = "Q"
MAX_PLYS = 40
pieceVals = {"p" : 1, "P" : 1, "r" : 4, "R" : 4, "b" : 3, "B" : 3, "n" : 2, "N" : 2, "q" : 9, "Q" : 9, "k": 10, "K" : 10, "." : 0}

def chess_isValid(intX, intY):
    """Helper function to check if indexes are in 5X6 board
    Args:
        intX - integer representing column
        intY - integer representing row
    Returns:
        boolean
    """
    if intX < 0:
            return False
            
    elif intX > 4:
            return False
    
    if intY < 0:
            return False
            
    elif intY > 5:
            return False
    
    return True

def convLoc(sqrStr):
    """ convert C2 -> (2,4)"""
    return (ord(sqrStr[0])-97, 6 - int(sqrStr[1]))

class State:
    """Represents a mini chess game"""
    def __init__(self, strGameState):
        """Initialize game
        Args:
            strGameState - 40-41 character game string
        Returns:
            None
        """
        self.whoseTurn = None #charcter W or B
        self.moveNum = None #int representing total (full) moves
        self.board = [] #2d array of characters

        #string contains the turn count and whose turn followed by the board
        stateArray = strGameState.split("\n")
        header = stateArray.pop(0)
        stateArray = stateArray[:-1]#last elemnt is empyt since ends in \n
        for row in stateArray:
            self.board.append(list(row))

        #single digit move coutner - 1 W
        if len(header) == 3:
            self.moveNum = int(header[0])
            self.whoseTurn = header[2]
        #double digit move counter - 11 B
        else:
            self.moveNum = int(header[0] + header[1])
            self.whoseTurn = header[3]
        self.evaluation = None
    
    def setScore(self, score):
        """Basic setter function"""
        self.evaluation = score
    
    def __str__(self):
        """Print board in same format as will be used by the framework
        Args:
            None
        Returns:
            string - game encoding
        """
        strEncoding = str(self.moveNum) + " " + self.whoseTurn + "\n"
        for row in self.board:
            strEncoding += "".join(row) + "\n"
        return strEncoding
    
    def getPieceColor(self, piece):
        """Helper function to check a character's side"""
        if piece.islower():
            return BLACK
        return WHITE

    def genMoves(self):
        """Create all possible moves for the state
        Args:
            None
        Returns:
            Array of move objects
        """
        moves = []
        for i in range(6):
            for j in range(5):
                if self.whoseTurn == self.getPieceColor(self.board[i][j]):
                    tempMoves = self.getPieceMoves(i, j)
                    moves += tempMoves
        return moves

    def moveScan(self, col0, row0, dCol, dRow, stopShort = False, capture = True):
        """Continue in a direction until hitting other obsticles"""
        #capture None == only in tutorial
        moves = []
        col = col0
        row = row0
        color = self.getPieceColor(self.board[row][col]) 
       
        while True:
            col += dCol
            row += dRow
            #out of bounds
            if not chess_isValid(col, row):
                break
            newLoc = self.board[row][col]
            
            #found obsticle, either take or stop looking
            if newLoc != BLANK:
                if color == self.getPieceColor(newLoc):
                    break
                if capture == False:
                    break
                stopShort = True
            
            elif capture == None:
                break
            moves.append(Move(Square(col0, row0), Square(col, row)))
            
            if stopShort:
                break
        return moves

    def symmscan(self, col, row, dCol, dRow, stopShort, capture):
        """Generates a call to move scan for each direction"""
        moves = []
        for i in range(1,5):
            moves += self.moveScan(col, row, dCol, dRow, stopShort, capture)
            temp = dCol
            dCol = dRow
            dRow = temp
            dRow = dRow * -1
        return moves

    def getPieceMoves(self, row, col):
        """Generates moveis for a particular location
        Args:
            row - int
            col - int
        Returns:
            Array of moves
        """    
        piece = self.board[row][col]
        if piece != "P":
            piece = piece.lower()

        moves = []
        if piece == "q" or piece == "k":
            stopShort = False
            if piece == "k":
                stopShort = True
            moves += self.symmscan(col, row, 0, 1, stopShort, True)
            moves += self.symmscan(col, row, 1, 1, stopShort, True)
        
        elif piece == "r" or piece == "b":
            stopShort = piece == "b"
            capture = piece == "r"
            moves += self.symmscan(col, row, 0, 1, stopShort, capture)
            if piece == "b":
                moves += self.symmscan(col, row, 1, 1, False, True)
        
        elif piece == "n":
            moves += self.symmscan(col, row, 1, 2, True, True)
            moves += self.symmscan(col, row, -1, 2, True, True)
        
        elif piece == "p" or piece == "P":
            dir = -1
            if piece == "p":
                dir = 1
            moves += self.moveScan(col, row, -1, dir, True, None)
            moves += self.moveScan(col, row, 1, dir, True, None)
            moves += self.moveScan(col, row, 0, dir, True, False)

        return moves
    
    def move(self, move):
        """Updates state based on move
        Args:
            move - Move object
        Returns:
            piece captured
        """
        pieceToMove = self.board[move.fromSquare.row][move.fromSquare.col]
        pieceTaken = self.board[move.toSquare.row][move.toSquare.col]
        origPieceToMove = pieceToMove # used to detect a catured piece

        #promote pawn that made it across the board
        if move.toSquare.row == 0 or move.toSquare.row == 5:
            if pieceToMove == B_PAWN:
                pieceToMove = B_QUEEN
            elif pieceToMove == W_PAWN:
                pieceToMove = W_QUEEN
        
        #replace to square with current piece and leave a blank behind
        self.board[move.fromSquare.row][move.fromSquare.col] = BLANK
        self.board[move.toSquare.row][move.toSquare.col] = pieceToMove

        #switch shose turn it is    
        if self.whoseTurn ==  BLACK:
            self.whoseTurn = WHITE
            self.moveNum += 1
        else:
            self.whoseTurn = BLACK

        #update eval based on taken piece and promotion
        self.evaluation += pieceVals[pieceTaken]
        self.evaluation += pieceVals[pieceToMove]
        self.evaluation -= pieceVals[origPieceToMove]
        
        #function internally updates the evaluation
        self.checkOver(pieceTaken)

        #since switched whose turn it is
        self.evaluation = -self.evaluation

        return (pieceTaken, origPieceToMove)

    def isOver(self):
        """Decides if the state is complete
        Args:
            None
        Returns:
            Boolean - True means game is over
        """
        if self.evaluation == WIN or self.evaluation == LOSS:
            return True
        if self.moveNum > 40:
            return True
        return False
    
    def checkOver(self, captured = None):
        """Detects if game is complete based without iterating over the board
        Args:
            captured - charcter that represents the taken piece
        Returns:
            Boolean - True means game has now over
        """
        result = False
        if self.moveNum > MAX_PLYS:
            self.evaluation = DRAW
            resut = True
        elif captured == B_KING or captured == W_KING:
            self.evaluation = WIN
            result = True
        elif self.moveNum == MAX_PLYS:
            self.evaluation = DRAW
            result = True
        return result

    def moveStr(self, moveString):
        """Converts framework string to a move object
        Args:
            moveString - String ex c2-c3
        Returns:
            piece captured
        """
        
        return self.move(makeMove(moveString))
    
    def undoMove(self, move, taken, movedPiece):
        """ Change to as if move did not occur
        Args:
            move - move object
            taken - piece that was at to square
        Returns:
            Boolean - False if was un able to prorerly do incrimental update
        """
        #score needs to reflect uncaptured and unpromoted pieces
        updated = True
        if self.isOver():
            updated = False
        else:
            promoted = self.board[move.toSquare.row][move.toSquare.col]
            self.evaluation += pieceVals[taken]
            self.evaluation += pieceVals[promoted]
            self.evaluation -= pieceVals[movedPiece]
        
        self.board[move.toSquare.row][move.toSquare.col] = taken
        self.board[move.fromSquare.row][move.fromSquare.col] = movedPiece
        
        #switch shose turn it is    
        if self.whoseTurn ==  BLACK:
            self.whoseTurn = WHITE
        else:
            self.moveNum -= 1
            self.whoseTurn = BLACK
       
        self.evaluation = -self.evaluation
        
        return updated
        


def makeMove(moveString):
    """ Convertes move string to move object"""

    fromStr, toStr = moveString.split("-")
    
    fromLoc = convLoc(fromStr)
    fromMove = Square(fromLoc[0], fromLoc[1])
    
    toLoc = convLoc(toStr)
    toMove = Square(toLoc[0], toLoc[1])
    
    return Move(fromMove, toMove)

class Square:
    def __init__(self, col ,row):
        """Identifes piece postion on board"""
        self.col = col
        self.row = row
    def __str__(self):
        return str(self.row) + "," + str(self.col)
    def toString(self):
        return chr(self.col + 97) + str(6 - self.row)

class Move:
    def __init__(self, fromSquare, toSquare):
        """captures the two squares used in a move
        Args:
            fromSquare = Square object where piece is currently
            toSqure - Square object where to place piece
        Returns:
            None
        """
        self.fromSquare = fromSquare
        self.toSquare = toSquare

    def __str__(self):
        return str(self.fromSquare) + "->" + str(self.toSquare)

    def toString(self):
        """Print in traditional chess move syntax"""
        return self.fromSquare.toString() + "-" + self.toSquare.toString() + "\n"

