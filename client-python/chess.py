INFINITY = 1000000000
WIN = INFINITY -1
LOSS = -INFINITY + 1
DRAW = 0 
import pickle

import random, os, sys, time, copy
from collections import OrderedDict
sys.path.append(os.path.dirname(__file__))
from board import *
from transposition import Trans, EXACT, LOWER, UPPER

#cached globals
trans = Trans()
state = None
moves = []#list of tuples (str move, piece taken)

def chess_reset():
    """reset the state of the game
    Args:
        None
    Returns:
        None
    """
    global trans
    trans.reset()
    chess_boardSet(getStartStr())

def chess_boardGet():
    """used by framewrok to check interal state
    Args:
        None
    Returns:
        string - 40-41 game encoding
    """
    return str(state)


def chess_boardSet(strIn):
    """change current state of the game
    Args:
        strIn - string representation of game
    Returns:
        None
    """
    global state
    state = State(strIn)
    state.setScore(chess_eval())
    moves = []

def chess_winner():
    """Using current state, check if game is over
    Args:
        None
    Returns:
        char - ?(no winner), =(draw), W(White wins), B(Black wins)
    """
    
    #game ends in draw if reach move cap
    if state.moveNum > 41:
        return '='

    #check which sides still has a king
    whiteKing = False
    blackKing = False
    for row in state.board:
        for cell in row:
            if cell == W_KING:
                whiteKing = True
            elif cell == B_KING:
                blackKing = True
    
    #king was captured by one side
    if not whiteKing:
        return BLACK
    if not blackKing:
        return WHITE

    if state.moveNum == 41:
        return "="

    #no result found. continue game
    return '?'


def chess_isValid(intX, intY):
    """Helper function to check if indexes are in 5X6 board
    Args:
        intX - integer representing column
        intY - integer representing row
    Returns:
        boolean
    """
    if intX < 0:
            return False
            
    elif intX > 4:
            return False
    
    if intY < 0:
            return False
            
    elif intY > 5:
            return False
    
    return True


def chess_isEnemy(strPiece):
    """checks if piece belongs to opponent
    Args:
        strPiece - character rep of a piece 
    Returns:
        boolean
    """
    if strPiece == BLANK:
        return False
    if state.whoseTurn == WHITE:
        return strPiece.islower()
    return strPiece.isupper()


def chess_isOwn(strPiece):
    """checks if piece belongs to self
    Args:
        strPiece - character rep of a piece 
    Returns:
        boolean
    """
    if strPiece == BLANK:
        return False
    if state.whoseTurn == BLACK:
        return strPiece.islower()
    return strPiece.isupper()

def chess_isNothing(strPiece):
    """checks if piece is empty
    Args:
        strPiece - character rep of a piece 
    Returns:
        boolean
    """
    return strPiece == BLANK

def chess_eval():
    """When game is not over, uses pieces value totals
    Args:
        None
    Returns:
        int - [-INF,INF]
    """
    #End game means no need to tally pieces
    winner = chess_winner()
    if winner == '=':
        return DRAW
    elif winner == state.whoseTurn:
        return WIN
    elif winner != '?':
        return LOSS

    #score is my piece total / all pieces on the board
    hisScore = 0
    myScore = 0

    #state eval is the difference in my pieces and his
    myScore = 0
    hisScore = 0
    for row in state.board:
        for piece in row:
            if chess_isNothing(piece):
                continue
            
            #positive means my piece
            val = pieceVals[piece]
            if chess_isEnemy(piece):
                hisScore += val
            else:
                myScore += val
    
    return myScore - hisScore

def chess_moves():
    """Generate list of moves. Note move is 6 characters
    Args:
        None
    Returns:
        Array of string moves
    """
    moves = state.genMoves()
    strOut = []
    for move in moves:
        strOut.append(move.toString())
    return strOut

def chess_movesShuffled():
    #randomly sort moves
    moves = chess_moves()
    random.shuffle(moves)
    return moves


def chess_movesEvaluated():
    #rank possible moves based on eval after move is made
    moves = chess_moves()
    
    #if game is over, no moves are possible
    if moves == []:
        return "aaaaa\n"
    
    evals = {}#movestr->eval (int)
    for move in moves:
        moveObj = makeMove(move)
        taken, piece = state.move(moveObj)
        evals[move] = state.evaluation 
        
        if not state.undoMove(moveObj, taken, piece):
            state.setScore(chess_eval())
    
    #shuffle, so eqaul states are even
    items = evals.items()
    random.shuffle(items)
    
    #make dictionary have the first key be the move with lowest eval
    return OrderedDict(sorted(items, key=lambda x: x[1])).keys()

def chess_move(strIn):
    # perform the supplied move and update the state of the game
    strIn = strIn[:6]
    taken, piece = state.moveStr(strIn)
    moves.append((strIn, taken, piece))

def chess_moveRandom():
    # perform a random move and return it
    move = chess_movesShuffled()[0]
    chess_move(move)
    return move


def chess_moveGreedy():
    # perform a greedy move and return it - one example output is given below
    move = chess_movesEvaluated()[0]
    chess_move(move)
    return move

def searchNegaMax(depth):
    #search limited by depth cap or if the game is over
    if depth == 0 or chess_winner() != '?':
        return chess_eval()

    score = -INFINITY
    for move in chess_movesShuffled():
        chess_move(move)
        score = max(score, -searchNegaMax(depth -1))
        chess_undo()
    return score


def chess_moveNegamax(intDepth, intDuration = -1):
    # perform a negamax move and return it - one example output is given below - note that you can call the the other functions in here
    bestMove = None
    score = -INFINITY
    for move in chess_movesEvaluated():
        chess_move(move)
        temp = -searchNegaMax(intDepth - 1)
        chess_undo()
        
        if temp > score:
            bestMove = move
            score = temp
    
    chess_move(bestMove)
    return bestMove

def searchAlphaBeta(depth, alpha, beta, zobristVal):
    alphaOrig = alpha
    if depth == 0 or state.isOver():
        return state.evaluation
    
    #already have value for this node
    key, zobDepth, nodeEval, flag = trans.getElement(zobristVal)
    if zobristVal == key and zobDepth >= depth:
        if flag == EXACT:
            return nodeEval
        elif flag == LOWER:
            alpha = max(alpha, nodeEval)
        else:
            beta = min(beta, nodeEval)
        if alpha >= beta:
            return nodeEval

    score = -INFINITY
    for move in chess_movesEvaluated():
        moveObj = makeMove(move)
        tempZobrist = trans.zobristUpdate(zobristVal, state, moveObj)
        
        taken, piece = state.move(moveObj)
        score = max(score, -searchAlphaBeta(depth -1, -beta, -alpha, tempZobrist))
        if not state.undoMove(moveObj, taken, piece):
            state.setScore(chess_eval())
        alpha = max(alpha, score)
        
        if(alpha >= beta):
            break
    
    #store the value found from search
    flag = EXACT
    if score <= alphaOrig:
        flag = UPPER
    elif score >= beta:
        flag = LOWER
    trans.updateTrans(zobristVal, depth, score, flag)
    
    return score

def chess_moveAlphabeta(intDepth, intDuration = 300000):
    # perform a alphabeta move and return it - one example output is given below - note that you can call the the other functions in here
    
    #handle tournament play. 
    #-depth means we go until we run out of time. total game time passed in
    startTime = time.time()
    depthCap = INFINITY
    if intDepth > 0:
        depthCap = intDepth
    else:
        intDepth = 1

    duration = float(intDuration/1000)/float(42-state.moveNum)
    
    print("state\n" + str(state))
    print("depth: " + str(intDepth))
    print("time: " + str(intDuration))
    print("Time left: " + str(duration))
    
    finalMove = None
    while True:
        alpha = -INFINITY
        beta = INFINITY
        bestMove = None
        
        zobristVal = trans.getZobristVal(state)
        for move in chess_movesEvaluated():
            #make move
            moveObj = makeMove(move)
            tempZobrist = trans.zobristUpdate(zobristVal, state, moveObj)
            taken, piece = state.move(moveObj)
            
            #traverse futher to find more accurate minimax value
            temp = -searchAlphaBeta(intDepth - 1, -beta, -alpha, tempZobrist)
            
            #unable to do incrimental update on score. do it the slow way
            if not state.undoMove(moveObj, taken, piece):
                state.setScore(chess_eval())
            
            if temp > alpha:
                bestMove = move
                alpha = temp
            
            #found garenteed result for the game. no reason to keep looking
            if alpha == beta:
                break
        
        finalMove = bestMove
         
        #figure out if there is enough time to search another node deeper
        endTime = time.time() - startTime
        
        #stop search due to time or depth cap
        if intDepth == depthCap or (endTime * 10) > duration: 
            break
        intDepth += 1

    print("\tDepth decided: " + str(intDepth))
    print("\t" + str(time.time() - startTime))
    print("\t" + str(finalMove))
    print("\t" + str(alpha))
    chess_move(finalMove)
    return finalMove

def chess_undo():
    #undo the last move and update the state of the game
    moveStr, taken, piece = moves.pop()
    moveObj = makeMove(moveStr)
    if not state.undoMove(moveObj, taken, piece):
        state.setScore(chess_eval())

def getStartStr():
    """Got sick of copy and pasting this"""
    strOut = ''
    strOut += '1 W\n'
    strOut += 'kqbnr\n'
    strOut += 'ppppp\n'
    strOut += '.....\n'
    strOut += '.....\n'
    strOut += 'PPPPP\n'
    strOut += 'RNBQK\n'
    return strOut
