import os, sys
sys.path.append(os.path.dirname(__file__))
import chess
from chess import *

def test():
    #make sure start state is encoded properly in state class
    strOut = getStartStr()
    stateStr = str(State(strOut))
    assert(len(strOut) == len(stateStr))
    assert(strOut == stateStr)

    #make sure double digit start states work
    strOut = "1" + strOut
    stateStr = str(State(strOut))
    assert(len(strOut) == len(stateStr))
    assert(strOut == stateStr)

    #test move
    chess_reset()
    chess_move('c2-c3')
    strOut = ''
    strOut += '1 B\n'
    strOut += 'kqbnr\n'
    strOut += 'ppppp\n'
    strOut += '.....\n'
    strOut += '..P..\n'
    strOut += 'PP.PP\n'
    strOut += 'RNBQK\n'
    print(strOut)
    print(chess.state)
    assert(str(chess.state) == strOut)

    #test eval
    strOut = ''
    strOut += '1 B\n'
    strOut += 'kq.nr\n'
    strOut += 'ppppp\n'
    strOut += '.....\n'
    strOut += '..P..\n'
    strOut += 'PP.PP\n'
    strOut += 'RNBQK\n'
    chess_reset()
    shouldBe0 = chess_eval()
    assert(shouldBe0 == 0)
    chess_boardSet(strOut)
    shouldBeNeg = chess_eval()
    assert(shouldBeNeg < 0) 
    strOut = ''
    strOut += '1 W\n'
    strOut += 'kq.nr\n'
    strOut += 'ppppp\n'
    strOut += '.....\n'
    strOut += '..P..\n'
    strOut += 'PP.PP\n'
    strOut += 'RNBQK\n'
    chess_boardSet(strOut)
    shouldBePos = chess_eval()
    assert(shouldBePos > 0)

    chess_boardSet("21 W\nk....\nPqp.p\n..R.P\npB.rK\nP..pP\n..Q..\n");

    #test moves eval
    moves = chess_moves()
    movesEval = chess_movesEvaluated()
    print(moves)
    print(movesEval)
    assert(len(moves) == len(movesEval))
    for move in moves:
        assert(move in movesEval)

    #test_undo
    chess_move("c1-b2\n");
    chess_move("d2-d1\n");
    chess_move("b2-d2\n");
    chess_move("d3-d5\n");
    chess_move("d2-d5\n");
    chess_move("b5-a5\n");
    chess_move("c4-c2\n");
    chess_move("d1-d3\n");
    chess_undo();
    chess_undo();
    chess_undo();
    chess_undo();
    chess_undo();
    chess_undo();
    board = chess_boardGet();
    print(chess.state)
    print(board)
    print("22 W\nk....\nPqp.p\n..R.P\npB.rK\nPQ..P\n...q.\n")
    assert(board == "22 W\nk....\nPqp.p\n..R.P\npB.rK\nPQ..P\n...q.\n")
    chess_boardSet("17 B\nr....\n..k.p\n.pQ.Q\n....n\nPp.P.\n.RBK.\n");
    chess_move("c5-d5\n")
    chess_move("e4-c2\n")
    chess_move("b4-b3\n")
    chess_move("c4-b5\n")
    chess_move("e5-e4\n")
    chess_move("d2-d3\n")
    chess_move("b3-c2\n")
    chess_move("d3-d4\n")
    chess_move("c2-d1\n")
    chess_undo()
    chess_undo()
    chess_undo()
    chess_undo()
    chess_undo()
    chess_undo()
    chess_undo()
    board = chess_boardGet()
    print(board)
    print("18 B\nr....\n...kp\n.pQ..\n....n\nPpQP.\n.RBK.\n")
    
    #transpotion table testing
    chess_reset()
    trans = Trans()

    zobristVal = trans.getZobristVal(chess.state)
    trans.updateTrans(zobristVal, 2, 100, EXACT)
    assert(trans.getEval(zobristVal, 2) == 100)

    trans.reset()
    zobristVal = trans.getZobristVal(chess.state)
    origState = copy.deepcopy(chess.state)
    move = chess_moveRandom()
    zobVal = trans.getZobristVal(chess.state)
    temp = trans.zobristUpdate(zobristVal,origState, makeMove(move))
    assert(temp == zobVal)

    #test AB
    for i in range(0, 10):
        chess_reset()
        chess_boardSet("18 B\nk..qr\n..p.p\n.....\nR.nP.\n.p.QP\n....K\n") 
        #print(getState())
        ab = chess_moveAlphabeta(2)
        chess_reset()
        chess_boardSet("2 W\nkqbnr\nppp.p\n...p.\nP....\n.PPPP\nRNBQK\n")
        ab = chess_moveAlphabeta(2)
        chess_reset()
        ab = chess_moveAlphabeta(2)
        chess_reset()
	chess_boardSet("19 B\nk.Qnr\nbp..p\np...p\n...p.\n...P.\nR...K\n")
        print("Original state")
        ab = chess_moveAlphabeta(2)
        print(move)
        
    games = 1
    chess.trans = Trans()
    chess_reset()
    chess_moveAlphabeta(-1, 300000)
    chess_undo()
    chess_moveAlphabeta(-1, 300000)
    chess_undo()
    chess_moveAlphabeta(-1, 300000)
    items = 0
    for cell in chess.trans.table:
        key, depth, val, flag = cell
        if cell[0] != None:
            items += 1
    print("tableUse: " + str(items) + " - " + str(len(chess.trans.table)))
    
    chess.trans = Trans()
    print("AB vs greedy. All wins")
    ABWins, ABTime = challenge(chess_moveAlphabeta, chess_moveGreedy, -1, games, 0)
    assert(ABWins == games)
    assert(ABTime < 350)
    items = 0
    for cell in chess.trans.table:
        key, depth, val, flag = cell
        if key != None:
            items += 1
    print("tableUse: " + str(items) +  " - " + str(len(chess.trans.table)))

    chess.trans = Trans()
    print("AB vs greedy. All wins")
    ABWins, ABTime = challenge(chess_moveAlphabeta, chess_moveGreedy, 6, games, 0)
    assert(ABWins == games) 
    print("negamax vs greendy. all wins")
    NMWins, NMTime = challenge(chess_moveNegamax, chess_moveGreedy, 4, games,  0)
    assert(NMWins == games)

    games = 1
    chess.trans = Trans()
    print("AB vs NM. should be evern")
    ABWins, ABTime = challenge(chess_moveAlphabeta, chess_moveNegamax, 5, games, 0)
    #assert(0 < sys.getsizeof(trans.table) <= 100000)
    items = 0
    for cell in chess.trans.table:
        key, depth, val, flag = cell
        if key != None:
            items += 1
    print("tableUse: " + str(items) +  " - " + str(len(chess.trans.table)))

    chess.trans = Trans()
    print("AB vs NM. All wins")
    ABWins, ABTime = challenge(chess_moveAlphabeta, chess_moveNegamax, 6, games, 2)
    assert(ABWins == games)
    #assert(0 < sys.getsizeof(trans.table) <= 100000)

def challenge(player1, player2, depth = 4,matches = 20, handycap = 2):
    start = time.time()
    p1Wins = 0
    draws = 0
    for i in range(0,matches):
        chess_reset()
        while True:
            player1(depth)
            if chess_winner() != "?":
                break
            try:
                player2(depth - handycap)
            except:
                player2()
            if chess_winner() != "?":
                break
        if chess_winner() == WHITE:
            p1Wins += 1
        elif chess_winner() == "=":
            draws += 1
    totalTime = time.time() - start
    print("Draws: " + str(draws) + " P1 wins: " + str(p1Wins) + "of " + str(matches) + " in " + str(totalTime))
    return(p1Wins, totalTime) 
test()

